# Live Preview

Just run `make live-preview` in the project root folder. `serve` (that is automatically called by `watch`) will use `live-server` to serve this folder on port _:8080_.

- `build.js` -- Builds `index.html` and all the pages `page-%02d.jpg` with `pdftoppm`.

- `serve` -- This just calls `live-server` with some reasonable options.



