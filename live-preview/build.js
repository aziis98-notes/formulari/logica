const fs = require('fs/promises');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const indexHtml = (pages) => `
	<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Live Preview PDF</title>

    <style>
        * {
            box-sizing: border-box;
        }

        html, body {
            margin: 0;
            padding: 0;

            min-width: 100%;
            min-height: 100%;
        }

        body {
            padding-bottom: 25vh;
            background: #f0f0f0;
        }

        img {
            display: block;
            width: 100%;
            border-bottom: 2px solid #ddd;
        }
    </style>
</head>
<body>
    ${pages.map((page, index) => `<img src="${page}" alt="${index + 1}">`).join('\n    ')}
</body>
</html>
`;

async function run(command) {
    console.log(`Running "${command}"`);
    await exec(command);
} 

async function main() {
	// Generate all the images
	await run(`pdftoppm -jpeg -r 300 main.pdf live-preview/page`);

    // Retrive generated files
	const files = (await fs.readdir('live-preview')).filter(f => f.startsWith('page'));

    // Generate the html page
    console.log('Generating "live-preview/index.html"');
    await fs.writeFile('live-preview/index.html', indexHtml(files), 'utf8');
}

main();
