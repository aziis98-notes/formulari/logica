
# Formulario di Logica

Versione più recente del PDF: <https://gitlab.com/aziis98-notes/formulari/logica/-/raw/main/main.pdf>

Generated from template: <https://github.com/aziis98/template-latex>

## Structure

All _source files_ should be placed in [`src/`](src), the output PDF is [`main.pdf`](main.pdf).

## Usage

To be organized I've recently started using this Makefile to manage LaTeX projects, so just use `make`, `make all` or `make main.pdf` to build the project and generate the output PDF.

### Live reload with entr

If you have `entr` installed you can also have "live reload" the pdf on save with

```bash
./watch
```

that will automatically rebuild [`main.pdf`](main.pdf).

### Preview

This command generates jpg images for every page of the pdf and a webpage to preview in localhost the project (on a tablet in the same LAN).

```bash
./watch --preview
```


## More Commands

### Extract the _Table of Contents_

```bash
grep 'section' src/main.tex | grep -v '%' | column
```


