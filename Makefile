
.PHONY: all clean live-preview

# Utility Targets

all: main.pdf live-preview

live-preview: live-preview/index.html

clean:
	rm -rf .cache
	rm -f main.pdf

# Real Targets

main.pdf: .cache src/main.tex src/prelude.tex
	cp -vu -a src/* .cache/
	cd .cache; latexmk -pdf -halt-on-error main.tex
	cp -vu .cache/main.pdf main.pdf

live-preview/index.html: main.pdf
	node live-preview/build.js
	# Ehm, è meglio averlo per Make mentre si testa
	touch live-preview/index.html

.cache:
	mkdir -p .cache

